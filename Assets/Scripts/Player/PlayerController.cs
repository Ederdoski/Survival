﻿using System.Collections;
using System.Collections.Generic;
using TouchControlsKit;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public string Scene;

    private float triggerRound;
    private float rotation;
    private float cdFade;

    private bool prevGrounded;
    private bool isChangeScene;
    private bool binded;

    public Text tHeal;
    public Text tKills;
    public Text tRound;

    private GameObject skill;

    private Transform myTransform, cameraTransform;

    private CharacterController controller;

	private Shoot shootObject;

    void Awake()
    {
        isChangeScene = false;
        myTransform = transform;
        cameraTransform = Camera.main.transform;
        controller = GetComponent<CharacterController>();
    }

    void Start()
    {
        SendMessage("startFade", true);

        tHeal.text  = "Vida : " + PlayerUI.getHeal();
        tKills.text = "Muertes : " + PlayerUI.getKill();
        tRound.text = "Ronda : " + PlayerUI.getRound();

        triggerRound = Constants.ROUND_REFRESH;
    }

    void Update()
    {
        cdFade = Coldown.add(cdFade);

        setKills();
        setHeal();
        setRound();
        touchPadCameraListener();
        listenerDead();
        listenerBuff();
    }

    void FixedUpdate()
    {
        touchPadMovementListener();
    }

    private void touchPadMovementListener()
    {
        Vector2 move = TCKInput.GetAxis("Joystick");
        PlayerMovement(move.x, move.y);
    }

    private void touchPadCameraListener()
    {
        Vector2 look = TCKInput.GetAxis("Touchpad");
        PlayerRotation(look.x, look.y);
    }

    private void PlayerMovement(float horizontal, float vertical)
    {
        bool grounded = controller.isGrounded;

        Vector3 moveDirection = myTransform.forward * vertical * Constants.SPEED_PLAYER;
        moveDirection += myTransform.right * horizontal;

        moveDirection.y = -10f;

        if (grounded)
            moveDirection *= 7f;

        controller.Move(moveDirection * Time.fixedDeltaTime);

        if (!prevGrounded && grounded)
            moveDirection.y = 0f;

        prevGrounded = grounded;
    }

    public void PlayerRotation(float horizontal, float vertical)
    {
        myTransform.Rotate(0f, horizontal * 12f, 0f);
        rotation += vertical * 12f;
        rotation = Mathf.Clamp(rotation, -60f, 60f);
        cameraTransform.localEulerAngles = new Vector3(-rotation, cameraTransform.localEulerAngles.y, 0f);
    }

    private void listenerBuff(){

        if(PlayerUI.getActiveBuff().Count > 0)
        {
            for(int i=0; i < PlayerUI.getActiveBuff().Count; i++){
                if(PlayerUI.getActiveBuff()[i].getTimeEnd() <= Time.time){
                    PlayerUI.removeBuff(PlayerUI.getActiveBuff()[i].getName(), i);
                }

            }
        }
    }

    private void setRound()
    {
        if (PlayerUI.getKill() - triggerRound == 0)
        {
            triggerRound = triggerRound + Constants.ROUND_REFRESH;
            PlayerUI.addRound();
            tRound.text = "Ronda : " + PlayerUI.getRound();

            if(Constants.TIME_SPAWN > Constants.MIN_TIME_SPAWN){
                Constants.TIME_SPAWN--;
            }
        }
    }

    private void setHeal()
    {
        if (PlayerUI.getHeal() >= 1)
        {
            tHeal.text = "Vida : " + PlayerUI.getHeal();
        }

        if (PlayerUI.getHeal() == 0 && !isChangeScene)
        {
            isChangeScene = true;
            tHeal.text = "Vida : 0";
            cdFade = Coldown.reset();
            SendMessage("startFade", false);
        }
    }

    public void setKills()
    {
        tKills.text = "Muertes : " + PlayerUI.getKill();
    }

    void listenerDead(){

        if(isChangeScene && cdFade > 3)
        {
            isChangeScene = false;
            SceneManager.LoadScene(Scene, LoadSceneMode.Single);
        }
    }
}

