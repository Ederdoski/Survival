﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreUI : MonoBehaviour {

    public Text kills;
    public Text round;
    public Text bossKill;


    void Start () {
        kills.text = "Muertes : " + PlayerUI.getKill();
        round.text = "Ronda : " + PlayerUI.getRound();
        bossKill.text = "Jefes : " + PlayerUI.getBossKill();

        if (PlayerUI.getKill() >= PlayerPrefs.GetInt("score"))
        {
            PlayerPrefs.SetInt("score", PlayerUI.getKill());
        }

        if (PlayerUI.getRound() >= PlayerPrefs.GetInt("round"))
        {
            PlayerPrefs.SetInt("round", PlayerUI.getRound());
        }

        if (PlayerUI.getBossKill() >= PlayerPrefs.GetInt("bossKill"))
        {
            PlayerPrefs.SetInt("bossKill", PlayerUI.getBossKill());
        }
    }

}
