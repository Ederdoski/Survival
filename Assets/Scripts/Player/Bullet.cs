﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public GameObject animExplotion;
    public GameObject animHitWhite;
    public GameObject animHitSparkExplotion;

    void FixedUpdate()
    {
        if (Shoot.getTypeGun() == Constants.GUN_SHOTGUN)
        {
            GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + transform.forward * Constants.SPEED_BULLET_SHOTGUN * Time.deltaTime);
        }

        if (Shoot.getTypeGun() == Constants.GUN_AK47)
        {
            GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + transform.forward * Constants.SPEED_BULLET_AK47 * Time.deltaTime);
        }

        if (Shoot.getTypeGun() == Constants.GUN_GRENADE_LAUNCHER)
        {
            GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + transform.forward * Constants.SPEED_GRENADE_LAUNCHER * Time.deltaTime);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Wall" || other.gameObject.tag == "Enemy")
        {
            if(Shoot.getTypeGun() == Constants.GUN_SHOTGUN)
            {
                GameObject ex = Instantiate(animHitSparkExplotion, transform.position, transform.rotation) as GameObject;
                Destroy(ex.gameObject, Constants.DESTROY_EXPLODE_SHOTGUN);
            }

            if(Shoot.getTypeGun() == Constants.GUN_AK47)
            {
                GameObject ex = Instantiate(animHitWhite, transform.position, transform.rotation) as GameObject;
                Destroy(ex.gameObject, Constants.DESTROY_EXPLODE_SHOTGUN);
            }

            if(Shoot.getTypeGun() == Constants.GUN_GRENADE_LAUNCHER)
            {
                GameObject ex = Instantiate(animExplotion, transform.position, transform.rotation) as GameObject;
                Destroy(ex.gameObject, Constants.DESTROY_EXPLODE_SHOTGUN);
            }

            destroyBullet();
        }
    }

    void destroyBullet()
    {
        Destroy(gameObject, Constants.DESTROY_BULLET);
    }
}
