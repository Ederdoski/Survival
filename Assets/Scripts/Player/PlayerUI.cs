﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerUI : MonoBehaviour {

    public static List<arrayBuff> activeBuff = new List<arrayBuff>();
	public static List <weaponObject> aWeapons = new List<weaponObject>();

    public static float heal = Constants.PLAYER_HEAL;
    public static int kills  = 0;
    public static int round  = 1;
    public static int bossKill  = 0;
	public static int weapons  = 1;

    private static bool buffShield = false;
    private static bool buffSpeed = false;
    private static bool buffGun = false;

    public static void addKill()
    {
        kills++;
    }

    public static void addRound()
    {
        round++;
    }

    public static void addBossKill()
    {
        bossKill++;
    }

	public static void addWeapon()
	{
		weapons++;
	}

    public static void addDamage(float damage)
    {
        if(!buffShield){
            if (heal >= 1)
            {
                heal = heal - damage;
                Vibration.Vibrate(500);
            }
            else
            {
                heal = 0;
                Vibration.Vibrate(2000);
            }
        }
    }

    private static void addOrModifyBuff(string id, string name){

        if(PlayerUI.getActiveBuff().Count > 0){

            for(int i=0; i < PlayerUI.getActiveBuff().Count; i++){
            
                if(activeBuff[i].getID() == id){
                    activeBuff[i].setTimeEnd(Time.time + Constants.TIME_DURATION_BUFF);
                }
            
            }

        }else{
            activeBuff.Add(new arrayBuff(id, name, Time.time, Time.time + Constants.TIME_DURATION_BUFF)); 
        }
    }

    public static void addBuff(string typeBuff){
        
        if(typeBuff == Constants.SHIELD){
            BuffControl BCshield = GameObject.Find ("buffShieldIU").GetComponent<BuffControl> ();
            BCshield.activeBuffUI();

            if(!buffShield){
                buffShield = true;
            }

            addOrModifyBuff(Constants.ID_BUFF_SHIELD, Constants.SHIELD);
        }

        if(typeBuff == Constants.SPEED){
            BuffControl BCspeed = GameObject.Find ("buffSpeedIU").GetComponent<BuffControl> ();
            BCspeed.activeBuffUI();

            if(!buffSpeed){
                buffSpeed = true;
                Constants.SPEED_PLAYER = Constants.SPEED_PLAYER + Constants.GAIN_BUFF_SPEED;
            }

            addOrModifyBuff(Constants.ID_BUFF_SPEED, Constants.SPEED);
        }

        if(typeBuff == Constants.BULLET){
            BuffControl BCgun = GameObject.Find ("buffBulletIU").GetComponent<BuffControl> ();
            BCgun.activeBuffUI();

            if(!buffGun){
                buffGun = true;
            }

            addOrModifyBuff(Constants.ID_BUFF_BULLET, Constants.BULLET);
        }

        if(typeBuff == Constants.HEAL){
            
            if(heal + Constants.GAIN_BUFF_HEAL > Constants.PLAYER_HEAL){
                heal = Constants.PLAYER_HEAL;
            }else{
                heal = heal + Constants.GAIN_BUFF_HEAL;
            }
        }
    }

	public static void instantiateWeapons(List <weaponObject> aWeaponsTemp){
		if (aWeapons.Count == 0) {
			for (int i = 0; i < aWeaponsTemp.Count; i++) {

				aWeapons.Add (new weaponObject (aWeaponsTemp [i].getID (), aWeaponsTemp [i].getName (), aWeaponsTemp [i].getStatus (), aWeaponsTemp [i].getIsSelected (), 
					aWeaponsTemp [i].getCDWeapon (), aWeaponsTemp [i].getRewardWeapon (), aWeaponsTemp [i].getSoundFire (), aWeaponsTemp [i].getSoundReload (), aWeaponsTemp [i].getBulet (), aWeaponsTemp [i].getWeapon()));
			}
		}
	}

    public static int getKill()
    {
        return kills;
    }

    public static float getHeal()
    {
        return heal;
    }

    public static int getRound()
    {
        return round;
    }

    public static int getBossKill()
    {
        return bossKill;
    }

	public static int getWeapons()
	{
		return weapons;
	}

    public static List<arrayBuff> getActiveBuff()
    {
        return activeBuff;
    }


    public static void removeBuff(string typeBuff, int index){

        if(typeBuff == Constants.SHIELD){
            BuffControl BCshield = GameObject.Find ("buffShieldIU").GetComponent<BuffControl> ();
            BCshield.disableBuffUI();
            buffShield = false;  
        }

        if(typeBuff == Constants.SPEED){
            BuffControl BCspeed = GameObject.Find ("buffSpeedIU").GetComponent<BuffControl> ();
            BCspeed.disableBuffUI();
            Constants.SPEED_PLAYER = Constants.SPEED_PLAYER - Constants.GAIN_BUFF_SPEED;
            buffSpeed = false;
        }

        if(typeBuff == Constants.BULLET){
            BuffControl BCgun = GameObject.Find ("buffBulletIU").GetComponent<BuffControl> ();
            BCgun.disableBuffUI();
        }

        activeBuff.RemoveAt(index);
        
    }

    public static void reset()
    {
        heal  = 100;
        kills = 0;
        round = 1;
    }
}
