﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddWeapons : MonoBehaviour
{
    public string weapon;

    public Text txtUI;

    private bool isText;

    private float coldown;

    void Start()
    {
        txtUI.text = "";
        isText = false;
    }

    void Update()
    {
        coldown = Coldown.add(coldown);

        if (isText && coldown > 3)
        {
            txtUI.text = "";
            isText = false;
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.tag == Constants.PLAYER)
        {
			for (int i = 0; i < PlayerUI.aWeapons.Count; i++) {
				
				if (weapon == PlayerUI.aWeapons[i].getName()) 
				{
					if(PlayerUI.getKill() >= PlayerUI.aWeapons[i].rewardWeapon)
					{
						if (PlayerUI.aWeapons [i].getStatus ().Equals (Constants.FALSE)) {
							PlayerUI.addWeapon ();
							txtUI.text = PlayerUI.aWeapons[i].getName()+" Adquirida";

							for (int j = 0; j < PlayerUI.aWeapons.Count; j++) {
								if (PlayerUI.aWeapons [j].getName ().Equals (PlayerUI.aWeapons[i].getName())) {
									PlayerUI.aWeapons [j].setStatus (Constants.TRUE);
									break;
								}
							}
						}
					}
					else
					{
						txtUI.text = "Necesitas "+ PlayerUI.aWeapons[i].rewardWeapon + " muertes para canjear";
					}

					isText = true;
					coldown = Coldown.reset();
				}
			}
        }
    }

}
