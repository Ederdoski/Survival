﻿using System.Collections;
using System.Collections.Generic;
using TouchControlsKit;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TriggerBoss : MonoBehaviour {

    public GameObject menuUI;
    public GameObject BtnPositiveUI;
    public GameObject BtnNegativeUI;

    public Text txtUI;

    public static bool goToBoss;
    private bool goToArea;
    private bool goToSurvival;

    private float coldown;

    public string typeTrigger;

    void Awake(){
    	goToArea = false;
		goToBoss = false;
		goToSurvival = false;
    }

	void Start () {
		showMenu(false);
        txtUI.text = "";
	}
	
	void Update () {
		coldown = Coldown.add(coldown);
		listenerButton();
	}

	void showMenu(bool value){
        menuUI.SetActive(value);
        BtnPositiveUI.SetActive(value);
        BtnNegativeUI.SetActive(value);
	}


	void listenerButton()
	{

        if (TCKInput.GetAction("BtnMenuPositive", EActionEvent.Press) && coldown > 0.5f)
        {
        	showMenu(false);
        	coldown = Coldown.reset();

        	if(goToArea){
        		PlayerPrefs.SetString("goToScene", Constants.SCENE_BOSS_AREA);
            	SceneManager.LoadScene("loading", LoadSceneMode.Single);
        	}

        	if(goToBoss){
        		PlayerPrefs.SetString("goToScene", Constants.SCENE_BOSS_FIGHT);
            	SceneManager.LoadScene("loading", LoadSceneMode.Single);
        	}

        	if(goToSurvival){
        		PlayerPrefs.SetString("goToScene", Constants.SCENE_SURVIVAL);
            	SceneManager.LoadScene("loading", LoadSceneMode.Single);
        	}


        }

        if (TCKInput.GetAction("BtnMenuNegative", EActionEvent.Press) && coldown > 0.5f)
        {
        	showMenu(false);
        	coldown = Coldown.reset();
        }
	}


	 void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.tag == Constants.PLAYER)
        {
        	if(typeTrigger == Constants.ENTRY_BOSS){
             	txtUI.text = "Si completas el desafio obtendras una recompensa especial, ¿Aceptas el reto?";
             	goToArea = true;
        	}

        	if(typeTrigger == Constants.EASY_BOSS){
             	txtUI.text = "Si logras derrotar a -Dragon- obtendras un cofre con recompensas Especiales";
        		goToBoss = true;
        	}

        	if(typeTrigger == Constants.MEDIUN_BOSS){
             	//txtUI.text = "Si logras derrotar a -NameBoss- obtendras un cofre con recompensas Epicas";
             	txtUI.text = "Por el momento no posees acceso a este sitio";
        	}

        	if(typeTrigger == Constants.HARD_BOSS){
             	//txtUI.text = "Si logras derrotar a -NameBoss- obtendras un cofre con recompensas Legendarias";
             	txtUI.text = "Por el momento no posees acceso a este sitio";
          	}

          	if(typeTrigger == Constants.SCENE_SURVIVAL){
             	txtUI.text = "Has demostrado tu fortaleza vuelve a la arena y ve por tu recompensa";
             	goToSurvival = true;
          	}

			showMenu(true);
        }
    }
}
