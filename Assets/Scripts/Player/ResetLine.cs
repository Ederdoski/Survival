﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetLine : MonoBehaviour {

	private GameObject target;

	void OnTriggerEnter(Collider collider)
    {
    	if(collider.gameObject.tag == Constants.PLAYER)
    	{
        	target = GameObject.FindGameObjectWithTag(Constants.PLAYER);

    		target.transform.position = new Vector3(0, 0.77f, -7.34f);
    	}
    }
}
