﻿using System.Collections;
using System.Collections.Generic;
using TouchControlsKit;
using UnityEngine;
using UnityEngine.UI;

public class Shoot : MonoBehaviour {

	public List <weaponObject> aWeaponsTemp = new List<weaponObject>();

    private GameObject skill;

    private AudioSource audio;

    public Text txtUI;

    private bool isText;

    private float coldown;

    public static string typeGun;

	private string lastWeaponActive = "NULL";

    void Start()
    {		
        audio  = GetComponent<AudioSource>();
        skill  = null;
        isText = false;

		if (PlayerUI.aWeapons.Count > 0) {

			for (int i = 0; i < aWeaponsTemp.Count; i++) {

				Debug.Log (aWeaponsTemp [i].getWeapon ());
				PlayerUI.aWeapons[i].setWeapon(aWeaponsTemp [i].getWeapon ());			
			}
			PlayerUI.instantiateWeapons (PlayerUI.aWeapons);
		} else {
			PlayerUI.instantiateWeapons(aWeaponsTemp);
		}

		setActWeapon();
    }

    void Update()
    {
        coldown = Coldown.add(coldown);

        listenerTrigger();
        listenerChangeGun();

        if (isText && coldown > 2)
        {
            txtUI.text = "";
            isText = false;
        }
    }

    private void listenerTrigger()
    {
		for (int i=0; i<PlayerUI.aWeapons.Count; i++) {
			if (getTypeGun() == PlayerUI.aWeapons[i].getName() && TCKInput.GetAction("fireBtn", EActionEvent.Press) && coldown > PlayerUI.aWeapons[i].getCDWeapon())
			{
				shoot();
				i = PlayerUI.aWeapons.Count;
			}
		}
    }

    private void listenerChangeGun()
    {
        if (TCKInput.GetAction("changeGun", EActionEvent.Press) && coldown > 0.5f)
        {
			int positionMax = PlayerUI.aWeapons.Count-1;
			int positionSig = 0;

			for (int i=0; i<PlayerUI.aWeapons.Count; i++) 
			{
				if (getTypeGun().Equals(PlayerUI.aWeapons[i].getName ())) 
				{
					for (int j = i; j < PlayerUI.aWeapons.Count; j++) 
					{
						positionSig = j+1;

						if (positionSig <= positionMax) {

							if (PlayerUI.aWeapons [j + 1].getStatus ().Equals (Constants.TRUE)) {

								PlayerUI.aWeapons [i].getWeapon ().SetActive (false);
								PlayerUI.aWeapons [i].setIsSelected (Constants.FALSE);
							
								PlayerUI.aWeapons [j + 1].setIsSelected (Constants.TRUE);
								soundWeapon (PlayerUI.aWeapons [j + 1].getSoundReload ());
								setTypeGun (PlayerUI.aWeapons [j + 1].getName (), j + 1);
								break;
							}

						} else {
							if (PlayerUI.aWeapons [0].getStatus ().Equals (Constants.TRUE)) {

								PlayerUI.aWeapons [i].getWeapon ().SetActive (false);
								PlayerUI.aWeapons [i].setIsSelected (Constants.FALSE);

								PlayerUI.aWeapons [0].setIsSelected (Constants.TRUE);
								soundWeapon (PlayerUI.aWeapons [0].getSoundReload ());
								setTypeGun (PlayerUI.aWeapons [0].getName (), 0);
								break;
							}
						}
					}
					break;
				}
			}
            coldown = Coldown.reset();
        }
    }

	void setActWeapon(){
		for (int i = 0; i < PlayerUI.aWeapons.Count; i++) {
			if(PlayerUI.aWeapons[i].getStatus().Equals(Constants.TRUE) && PlayerUI.aWeapons[i].getIsSelected().Equals(Constants.TRUE))
			{
				setTypeGun(PlayerUI.aWeapons[i].getName(), i);
				i = PlayerUI.aWeapons.Count;
			}
		}
	}

    void shoot()
    {
		for (int i = 0; i < PlayerUI.aWeapons.Count; i++) 
		{
			if (getTypeGun() == PlayerUI.aWeapons[i].getName()) 
			{
				soundWeapon(PlayerUI.aWeapons[i].getSoundFire());

				skill = Instantiate(PlayerUI.aWeapons[i].getBulet(), GameObject.Find(PlayerUI.aWeapons[i].getName()+"Launcher").transform.position, transform.rotation) as GameObject;
				skill.name = "Bullet_"+PlayerUI.aWeapons[i].getName();
				i = PlayerUI.aWeapons.Count;
			}
		}

        coldown = Coldown.reset();
    }

	private void setTypeGun(string _typeGun, int position)
    {
        typeGun = _typeGun;
		PlayerUI.aWeapons[position].getWeapon().SetActive(true);
    }

    public static string getTypeGun()
    {
        return typeGun;
    }

	private void soundWeapon(AudioClip sound)
	{
		audio.PlayOneShot(sound, 0.5F);
	}

}
