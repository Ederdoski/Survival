﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Buff : MonoBehaviour {

	public string typeBuff;

	void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == Constants.PLAYER)
        {
        	PlayerUI.addBuff(typeBuff);
        	Destroy(gameObject);
        }
    }
}
