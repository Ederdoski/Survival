﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrollIA : MonoBehaviour {

    public TrollAnimation eAnimation;

    private RaycastHit hitInfo;

    private bool isFirtsTime;

    private float heal;
    private float coldown;

    private GameObject target;

    void Start()
    {
        isFirtsTime = true;
        heal = Constants.TROLL_HEAL;
    }

    void Update()
    {
            coldown = Coldown.add(coldown);

            findTarget();

            run();

            attack();
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == Constants.BULLET_SHOTGUN || collider.gameObject.tag == Constants.BULLET_AK47 || collider.gameObject.tag == Constants.BULLET_GRENADE)
        {
            if (heal > 0)
            {

                if (collider.gameObject.tag == Constants.BULLET_SHOTGUN)
                {
                    heal = heal - Constants.DAMAGE_BULLET_SHOTGUN;
                }

                if (collider.gameObject.tag == Constants.BULLET_AK47)
                {
                    heal = heal - Constants.DAMAGE_BULLET_AK47;
                }

                if (collider.gameObject.tag == Constants.BULLET_GRENADE)
                {
                    heal = heal - Constants.DAMAGE_GRENADE_LAUNCHER;
                }

            }

            if (heal > 0)
            {
                getHit();
            }
            else
            {
                dead();
            }
        }
    }

    void findTarget()
    {
        target = GameObject.FindGameObjectWithTag(Constants.PLAYER);
        transform.LookAt(target.transform);  
    }

    void getHit()
    {
        eAnimation.getHit();
    }

    void dead()
    {
        eAnimation.die();
        PlayerUI.addKill();
        Destroy(this);
        Destroy(gameObject, Constants.DESTROY_BODY_ENEMY);
    }

    void run()
    {
        
       float dist = Vector3.Distance(transform.position, target.transform.position);

        if (dist > Constants.RANGE_TROLL_NORMAL)
        {
            transform.Translate(new Vector3(0, 0, 1) * Constants.MOVE_SPEED_TROLL * Time.deltaTime);
            
            if (coldown > 2f || isFirtsTime)
            {
                isFirtsTime = false;
                eAnimation.run();
                coldown = Coldown.reset();
            }
        }
    }

    void attack()
    {
        if (coldown > Constants.COLDOWN_ATTACK_TROLL)
        {
            Ray ray = new Ray(this.transform.position, this.transform.forward);
            if (Physics.Raycast(ray, out hitInfo, Constants.RANGE_TROLL_NORMAL))
            {
                if (hitInfo.collider.tag == Constants.PLAYER)
                {
                    eAnimation.attack(Random.Range(0, 2));
                    PlayerUI.addDamage(Constants.BASIC_DAMAGE_ENEMY);
                }
                coldown = Coldown.reset();
            }
        }
    }

}


