﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrollAnimationMenu : MonoBehaviour {

    public TrollAnimation eAnimation;

    private float coldown;

    private int random;

    void Start()
    {
        coldown = 3;
    }

    void Update () {

        coldown = Coldown.add(coldown);

        listenerAnimation();
    }

    void listenerAnimation()
    {
        random = Random.Range(0, 3);

        if (random == 0)
        {
            if (coldown > 5)
            {
                victory();
            }
        }

        if(random == 1)
        {
            if (coldown > 5)
            {
                attack(Random.Range(0, 2));
            }
        }

        if (random == 2)
        {
            if (coldown > 5)
            {
                eAnimation.idle();
                coldown = Coldown.reset();
            }
        }
    }

    void victory()
    {
        eAnimation.victory();
        coldown = Coldown.reset();
    }

    void attack(int type)
    {
        eAnimation.attack(type);
        coldown =  Coldown.reset();
    }

}
