﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vision : MonoBehaviour {

	private static bool _isVisible;

    public string _tag;

    void Start(){
        _isVisible = false;
    }

    public static bool isVisible(){
        return _isVisible;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == _tag)
        {
            _isVisible = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == _tag)
        {
            _isVisible = false;
        }
    }
}
