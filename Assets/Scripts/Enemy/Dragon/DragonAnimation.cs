﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonAnimation : MonoBehaviour {

 	//---Animator 

    public Animator anim;

    //---Acciones

    private static int[] attacks = new int[2];

    private static int _Idle;
    private static int _Run;
    private static int _Walk;
    private static int _Die;

    //---Sounds
    public AudioClip soundAttack;

    public AudioClip soundIdle;

    AudioSource audio;

    void Awake()
    {
        audio = GetComponent<AudioSource>();

        anim = GetComponent<Animator>();

        attacks[0] = Animator.StringToHash("Attack");
        
        _Die = Animator.StringToHash("Dead");
        _Idle = Animator.StringToHash("Idle");
        _Walk = Animator.StringToHash("Walk");
        _Run = Animator.StringToHash("Run");

    }

    public void attack()
    {
        anim.SetTrigger(attacks[0]);
        audio.PlayOneShot(soundAttack, 0.7F);
    }

    public void idle()
    {
        anim.SetTrigger(_Idle);
    }

    public void die()
    {
        anim.SetTrigger(_Die);
    }

    public void run()
    {
        anim.SetTrigger(_Run);
    }

    public void walk()
    {
        anim.SetTrigger(_Walk);
    }
}
