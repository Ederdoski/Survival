﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragonIA : MonoBehaviour {

    public DragonAnimation eAnimation;

    public GameObject [] points;

    public GameObject exitBoss;
    public GameObject auraFire;
    public GameObject shieldFire;
    public GameObject fireBall;

    public AudioClip soundInit;
    public AudioClip soundFireball;
    public AudioClip soundDie;

    AudioSource audio;

    private GameObject posTarget;
    private GameObject target;
    private RaycastHit hitInfo;

    public Text tHeal;

    private bool isFirtsTime;
    private bool isIdle;

    private float heal;
    private float coldown;
    private float cdAttackSpecial;
    private float cdFireBall;
    private float cdStates;

    private int attackState;
    private int follow;
    private int resetFollow;

    void Awake()
    {
        audio = GetComponent<AudioSource>();
    }

    void Start()
    {
        tHeal.text = Constants.DRAGON_HEAL+"";

        attackState = 3;
		follow = Random.Range(5, 25);
        resetFollow = Random.Range(follow, 35);

        auraFire.SetActive(true);
        heal = Constants.DRAGON_HEAL;
    	isIdle = false;
        isFirtsTime = true;
    }

     void Update()
    {
        cdAttackSpecial = Coldown.add(cdAttackSpecial);
        cdFireBall = Coldown.add(cdFireBall);
        coldown  = Coldown.add(coldown);
        cdStates = Coldown.add(cdStates);

        if(cdStates > 25){
        	
        	Constants.MOVE_SPEED_RUN_DRAGON = Constants.MOVE_SPEED_RUN_DRAGON;
        	
	        auraFire.SetActive(false);
        	cdStates = Coldown.reset();

        	if(heal <= Constants.DRAGON_HEAL / 2){
            	attackState = Random.Range(0, 4);
        	}else{
            	attackState = Random.Range(0, 3);
        	}

            if(attackState == 0){
    			Constants.MOVE_SPEED_RUN_DRAGON = Constants.MOVE_SPEED_RUN_DRAGON +1f;
            	follow = Random.Range(5, 25);
            	resetFollow = Random.Range(follow, 35);
            }

            if(attackState == 3){
            	eAnimation.idle();
            	auraFire.SetActive(true);
	    		shieldFire.SetActive(true);
    			shieldFire.transform.localScale = new Vector3(1, 1, 1);
    			auraFire.transform.localScale = new Vector3(1, 1, 1);
            }

        }

        if(attackState == 0)
        {

	        if(!auraFire.active && !isIdle){
	        	auraFire.SetActive(true);
	        }

	        if(cdAttackSpecial < follow){
		        findTarget();
		        run();
	    	}

	 		if(cdAttackSpecial > follow && !isIdle){
		        eAnimation.idle();
		        isIdle = true;
	        	auraFire.SetActive(false);
	    	}

			if(cdAttackSpecial > resetFollow){
				isIdle = false;
		        cdAttackSpecial = Coldown.reset();
	    	}

	    	if(!isIdle){
	            attack(Constants.ATTACK_SPECIAL, Constants.RANGE_DRAGON_SPECIAL, Constants.SPECIAL_DAMAGE_DRAGON);
        	}
	    }

	    if(attackState == 1)
	    {
	    	if(auraFire.active){
	        	auraFire.SetActive(false);
	        }

	    	findTarget();
		    run();

		    if(coldown > 2){
		    	attack(Constants.ATTACK_NORMAL, Constants.RANGE_DRAGON_NORMAL, Constants.BASIC_DAMAGE_DRAGON);
			}
	    }

	    if(attackState == 2)
	    {
	    	auraFire.SetActive(true);
	    	attack(Constants.ATTACK_ULTI, 0, 0);
	    }

	    if(attackState == 3)
	    {
	    	concentration();
	    }
    			
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == Constants.BULLET_SHOTGUN || collider.gameObject.tag == Constants.BULLET_AK47 || collider.gameObject.tag == Constants.BULLET_GRENADE)
        {
            if (heal > 0)
            {

                if (collider.gameObject.tag == Constants.BULLET_SHOTGUN)
                {
                    heal = heal - Constants.DAMAGE_BULLET_SHOTGUN;
        			tHeal.text = heal+"";
                }

                if (collider.gameObject.tag == Constants.BULLET_AK47)
                {
                    heal = heal - Constants.DAMAGE_BULLET_AK47;
        			tHeal.text = heal+"";
                }

                if (collider.gameObject.tag == Constants.BULLET_GRENADE)
                {
                    heal = heal - Constants.DAMAGE_GRENADE_LAUNCHER;
        			tHeal.text = heal+"";
                }

            }

            if (heal <= 0)
            {
				tHeal.text = "0";
    			dead();
                PlayerUI.addBossKill();
            }
        }
    }

    void findTarget()
    {
	    target = GameObject.FindGameObjectWithTag(Constants.PLAYER);
	    transform.LookAt(target.transform);
    }

    void dead()
    {
        audio.PlayOneShot(soundDie, 0.7F);
        eAnimation.die();
        Destroy(this);
        Destroy(gameObject, Constants.DESTROY_BODY_BOSS);
        exitBoss.SetActive(true);
    }

    void walk()
    {
    	float dist = Vector3.Distance(transform.position, target.transform.position);

        if (dist > Constants.RANGE_DRAGON_NORMAL)
        {
            transform.Translate(new Vector3(0, 0, 1) * Constants.MOVE_SPEED_WALK_DRAGON * Time.deltaTime);
            
            if (coldown > 3f || isFirtsTime)
            {
                isFirtsTime = false;
                eAnimation.walk();
                coldown = Coldown.reset();
            }
        }
    }

    void run(){
    	float dist = Vector3.Distance(transform.position, target.transform.position);

        if (dist > Constants.RANGE_DRAGON_NORMAL-1.5f)
        {
            transform.Translate(new Vector3(0, 0, 1) * Constants.MOVE_SPEED_RUN_DRAGON * Time.deltaTime);
            
            if (coldown > 2f || isFirtsTime)
            {
                isFirtsTime = false;
                eAnimation.run();
                coldown = Coldown.reset();
            }
        }
    }

  	public bool isMaxAure(Vector3 va){
    	return Vector3.SqrMagnitude(va - new Vector3(10f ,10f ,10f)) < 1f;
 	}

 	public bool isMinAure(Vector3 va){
    	return Vector3.SqrMagnitude(va - new Vector3(2f ,2f , 2f)) < 2f;
 	}

 	void concentration(){
	    
 		if(heal < 100){
 			heal = heal + 0.5f;
        	tHeal.text = heal+"";
  		}

		if(!isMaxAure(shieldFire.transform.localScale) && !isMaxAure(auraFire.transform.localScale)){
    		shieldFire.transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
    	}

    	if(isMaxAure(shieldFire.transform.localScale) && !isMaxAure(auraFire.transform.localScale)){
    		auraFire.transform.localScale += new Vector3(0.05f, 0.05f, 0.05f);
    	}

    	if(isMaxAure(auraFire.transform.localScale)){
    		shieldFire.transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f);
    	}

		if(isMinAure(shieldFire.transform.localScale) && isMaxAure(auraFire.transform.localScale)){
    		shieldFire.SetActive(false);
    	}
 	}

    void attack(string typeAttack, float rangeAttack, float damage)
    {
        Ray ray = new Ray(this.transform.position, this.transform.forward);

        if(typeAttack == Constants.ATTACK_SPECIAL){

	       	if (Physics.Raycast(ray, out hitInfo, rangeAttack))
	        {
	            if (hitInfo.collider.tag == Constants.PLAYER)
	            {
	                PlayerUI.addDamage(damage);
	            }
	            coldown = Coldown.reset();
	        }
        }

        if(typeAttack == Constants.ATTACK_NORMAL){

        	if (coldown > Constants.COLDOWN_ATTACK_DRAGON)
	        {
	            if (Physics.Raycast(ray, out hitInfo, rangeAttack))
	            {
	                if (hitInfo.collider.tag == Constants.PLAYER)
	                {
	                    eAnimation.attack();
	                    PlayerUI.addDamage(damage);
	                }
	                coldown = Coldown.reset();
	            }
	        }
        }

        if(typeAttack == Constants.ATTACK_ULTI){

	        if(cdFireBall > Constants.COLDOWN_ATTACK_DRAGON){

	        	findTarget();
		        GameObject skill = Instantiate(fireBall, transform.position, transform.rotation) as GameObject;
         		
         		audio.PlayOneShot(soundFireball, 0.7F);
				cdFireBall = Coldown.reset();
				eAnimation.attack();

			}
        }
    }

}