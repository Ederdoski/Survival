﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {

    public GameObject enemyTroll;

    public GameObject enemyLich;

	public float[] positionSpawn;

    private float timer;

	private Vector3 position;

    void Awake()
    {
        timer = Time.time + 5;
    }
		
    void Update()
    {
        if (timer < Time.time) {

			for (int i = 0; i < 5; i++) {

				position = new Vector3 (Random.Range (positionSpawn [0], positionSpawn [1]), 1.08f, Random.Range (positionSpawn [2], positionSpawn [3]));

				if (Random.Range (0, 11) < 10) {
					Instantiate (enemyTroll, position, transform.rotation);
				} else {
					Instantiate (enemyLich, position, transform.rotation);
				}
			}
			timer = Time.time + Constants.TIME_SPAWN;
        }
    }
}
