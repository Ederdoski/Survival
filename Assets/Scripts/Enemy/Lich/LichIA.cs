﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LichIA : MonoBehaviour {

    public LichAnimation eAnimation;

    public GameObject lighting01;

    public GameObject lighting02;

    private GameObject skill;

    private GameObject target;

    private GameObject launcher;

    private RaycastHit hitInfo;

    private float heal;

    private float coldown;

    private bool isFirtsTime;


    void Start()
    {
        skill = null;
        isFirtsTime = true;
        heal = Constants.LICH_HEAL;
    }

    void Update()
    {
        coldown = Coldown.add(coldown);

        findTarget();

        run();

        attack();
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == Constants.BULLET_SHOTGUN || collider.gameObject.tag == Constants.BULLET_AK47 || collider.gameObject.tag == Constants.BULLET_GRENADE)
        {
            if (heal > 0)
            {
                if (collider.gameObject.tag == Constants.BULLET_SHOTGUN)
                {
                    heal = heal - Constants.DAMAGE_BULLET_SHOTGUN;
                }

                if (collider.gameObject.tag == Constants.BULLET_AK47)
                {
                    heal = heal - Constants.DAMAGE_BULLET_AK47;
                }
                
                if (collider.gameObject.tag == Constants.BULLET_GRENADE)
                {
                    heal = heal - Constants.DAMAGE_GRENADE_LAUNCHER;
                }

                getHit();
            }

            if (heal <= 0)
            {
                dead();
            }
        }
    }

    void findTarget()
    {
        target = GameObject.FindGameObjectWithTag(Constants.PLAYER);
        transform.LookAt(target.transform);
    }

    void getHit()
    {
        eAnimation.getHit();
    }

    void dead()
    {
        eAnimation.die();
        PlayerUI.addKill();
        Destroy(this);
        Destroy(gameObject, Constants.DESTROY_BODY_ENEMY);
    }

    void run()
    {

        float dist = Vector3.Distance(transform.position, target.transform.position);

        if (dist > Constants.RANGE_LICH_NORMAL)
        {
            transform.Translate(new Vector3(0, 0, 1) * Constants.MOVE_SPEED_LICH * Time.deltaTime);

            if (coldown > 2f || isFirtsTime)
            {
                isFirtsTime = false;
                eAnimation.run();
                coldown = Coldown.reset();
            }
        }
    }

    void attack()
    {
        if (coldown > Constants.COLDOWN_ATTACK_LICH)
        {
            int random = Random.Range(0, 2);

            Ray ray = new Ray(this.transform.position, this.transform.forward);

            if (random == 0)
            {
                skill = Instantiate(lighting01, this.transform.position, this.transform.rotation) as GameObject;
                Destroy(skill, 1.1f);
            }
            else
            {
                skill = Instantiate(lighting02, this.transform.position, this.transform.rotation) as GameObject;
                Destroy(skill, 1.7f);
            }

            if (Physics.Raycast(ray, out hitInfo, Constants.RANGE_LICH_SPECIAL))
            {
                if (hitInfo.collider.tag == Constants.PLAYER)
                {
                    eAnimation.attack(random);
                    PlayerUI.addDamage(Constants.BASIC_DAMAGE_ENEMY);
                }
                coldown = Coldown.reset();
            }
        }
    }
}
