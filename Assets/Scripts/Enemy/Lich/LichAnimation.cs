﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LichAnimation : MonoBehaviour {

    //---Animator 

    public Animator anim;

    //---Acciones

    private static int[] attacks = new int[2];

    private static int _Idle;
    private static int _Run;
    private static int _Walk;
    private static int _GetHit;
    private static int _Victory;
    private static int _Die;

    //---Sounds
    public AudioClip soundVictory;

    public AudioClip soundAttack;

    public AudioClip soundAttack2;

    public AudioClip soundIdle;

    AudioSource audio;

    void Awake()
    {
        audio = GetComponent<AudioSource>();

        anim = GetComponent<Animator>();

        attacks[0] = Animator.StringToHash("Attack01");
        attacks[1] = Animator.StringToHash("Attack02");
        _Die = Animator.StringToHash("Die");
        _Run = Animator.StringToHash("Run");
        _Idle = Animator.StringToHash("Idle");
        _GetHit = Animator.StringToHash("GetHit");
        _Victory = Animator.StringToHash("Victory");
        _Walk = Animator.StringToHash("Walk");

    }

    public void attack(int typeAttack)
    {
        if (typeAttack == Constants.BASIC_ATTACK)
        {
            anim.SetTrigger(attacks[0]);
            audio.PlayOneShot(soundAttack, 0.7F);
        }

        if (typeAttack == Constants.SPECIAL_ATTACK)
        {
            anim.SetTrigger(attacks[1]);
            audio.PlayOneShot(soundAttack2, 0.7F);
        }
    }

    public void idle()
    {
        audio.PlayOneShot(soundIdle, 0.7F);
    }

    public void die()
    {
        anim.SetTrigger(_Die);
    }

    public void getHit()
    {
        anim.SetTrigger(_GetHit);
    }


    public void run()
    {
        anim.SetTrigger(_Run);
    }

    public void walk()
    {
        anim.SetTrigger(_Walk);
    }

    public void victory()
    {
        audio.PlayOneShot(soundVictory, 0.5F);
        anim.SetTrigger(_Victory);
    }
}
