﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class arrayBuff{
	
	public string id;
	public string name;
	public float timeInit;
	public float timeEnd;
	public bool status;

	public arrayBuff(){

	}
	
	public arrayBuff(string _id, string _name, float _timeInit, float _timeEnd)
    {
        id = _id;
        name = _name;
        timeInit = _timeInit;
        timeEnd  = _timeEnd;
    }

  	public string getID(){
    	return id;
    }

    public string getName(){
    	return name;
    }

    public float getTimeInit(){
    	return timeInit;
    }

    public float getTimeEnd(){
    	return timeEnd;
    }

    public bool getStatus(){
    	return status;
    }

    public void setTimeInit(float _timeInit){
    	timeInit = _timeInit;
    }

    public void setTimeEnd(float _timeEnd){
    	timeEnd = _timeEnd;
    }

}
