﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuffControl : MonoBehaviour {

    [SerializeField] private Image imgBuff;
    
    void Start(){
        disableBuffUI();
    }

	public void activeBuffUI(){
        imgBuff.enabled = true;
	}

	public void disableBuffUI(){
        imgBuff.enabled = false;
	}
}
