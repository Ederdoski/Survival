﻿using System.Collections;
using System.Collections.Generic;
using TouchControlsKit;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuInit : MonoBehaviour {

    public AudioSource soundEnvironment;
    public GameObject configurationUI;
    public GameObject scoreUI;
    public GameObject infoUI;

    public Text killText;
    public Text roundText;
    public Text bossKillText;

    public Slider volumen;

    private float coldown;

    private bool isChangeScene = false;

    void Start()
    {    	        
        soundEnvironment = GetComponent<AudioSource>();
        soundEnvironment.Play();

        configurationUI.SetActive(false);
        infoUI.SetActive(false);
        scoreUI.SetActive(false);

    }

    void OnGUI()
    {
        Constants.VOLUME_ACT = volumen.value;
        soundEnvironment.volume = Constants.VOLUME_ACT;
    }

    void Update () {

        coldown = Coldown.add(coldown);

        listenerPlay();
        listenerScore();
        listenerExit();
        listenerAbout();
        listenerConfiguration();  
        listenerChangeScene();      
    }

    void listenerAbout()
    {
        if (TCKInput.GetAction("AboutBtn", EActionEvent.Press) && coldown > 0.5f)
        {
            if(!infoUI.activeSelf)
            {
                infoUI.SetActive(true);
            }
            else
            {
                infoUI.SetActive(false);
            }
            coldown = Coldown.reset();
        }
    }

    void listenerConfiguration()
    {
        if (TCKInput.GetAction("OptionsBtn", EActionEvent.Press) && coldown > 0.5f)
        {
            if (!configurationUI.activeSelf)
            {
                configurationUI.SetActive(true);
            }
            else
            {
                configurationUI.SetActive(false);
            }
            coldown = Coldown.reset();
        }
    }

    void listenerChangeScene(){
    	if(isChangeScene)
    	{
        	SceneManager.LoadScene("loading", LoadSceneMode.Single);
    	}
    }

    void listenerPlay()
    {
        if (TCKInput.GetAction("StartGameBtn", EActionEvent.Press) && coldown > 0.5f)
        {

        	SendMessage("startFade", false);
        	isChangeScene = true;

            coldown = Coldown.reset();
            PlayerUI.reset();

        }
    }

    void listenerScore()
    {
        if (TCKInput.GetAction("ScoreBtn", EActionEvent.Press) && coldown > 0.5f)
        {

            coldown = Coldown.reset();

            if (!scoreUI.activeSelf)
            {
                scoreUI.SetActive(true);
                killText.text  = "Muertes: " + PlayerPrefs.GetInt("score");
                roundText.text = "Round: " + PlayerPrefs.GetInt("round");
                bossKillText.text = "Jefes: " + PlayerPrefs.GetInt("bossKill");
            }
            else
            {
                scoreUI.SetActive(false);
            }
        }
    }

    void listenerExit()
    {
        if (TCKInput.GetAction("ExitGameBtn", EActionEvent.Press) && coldown > 0.5f)
        {
            coldown = Coldown.reset();
            Application.Quit();
        }
    }
}
