﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Splash : MonoBehaviour {

    private float timer;


    private void Awake()
    {
        timer = Time.time + 3;
    }
    void Update () {

        if(timer < Time.time)
        {
            SceneManager.LoadScene("init",LoadSceneMode.Single);
        }

    }
}
