﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeScene : MonoBehaviour {

    public Image img;
    public GameObject fade;
    
    private void startFade(bool typeFade){
        fade.SetActive(true);
        StartCoroutine(fadeImage(typeFade));
    }

    IEnumerator fadeImage(bool fadeAway)
    {
        if (fadeAway)
        {
            for (float i = 1; i >= 0; i -= Time.deltaTime / 2)
            {
                img.color = new Color(1, 1, 1, i);
                yield return null;

            }
        }
        else
        {
            for (float i = 0; i <= 1; i += Time.deltaTime / 2)
            {
                img.color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
    }

}
