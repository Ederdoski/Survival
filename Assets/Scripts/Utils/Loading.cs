﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class Loading : MonoBehaviour {

	public Text tTips;

	private float coldownLoading;
	private float coldownTips;
	private bool isLoading;

	void Start()
	{
		isLoading = false;
        setTips();
	}

	 void Update()
    {
    	coldownLoading = Coldown.add(coldownLoading);
    	coldownTips = Coldown.add(coldownTips);
    	

        if (coldownLoading > Constants.TIME_SHOW_LOADING && !isLoading)
        {
            StartCoroutine(changeScene());
            isLoading = true;
            coldownLoading = Coldown.reset();
        }

		if(coldownTips > Constants.TIME_CHANGE_TIPS){
        	setTips();
    	}
    }
	
	private void setTips(){
        tTips.text = "Tip: "+Constants.ARRAY_TIPS[Random.Range(0, Constants.ARRAY_TIPS.Length)];
        coldownTips = Coldown.reset();
	}

	IEnumerator changeScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(PlayerPrefs.GetString("goToScene"));

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
