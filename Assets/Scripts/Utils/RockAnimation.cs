﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockAnimation : MonoBehaviour {

	public float maxHeight;
	public float minHeight;

    Rigidbody rock;

    double position;

    bool repeat = false;

    // Use this for initialization
	void Start () {
        rock = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {

        position = System.Math.Round(rock.transform.position.y, 1);
        
        if (position <= maxHeight && !repeat)
        {
            rock.MovePosition(GetComponent<Rigidbody>().position + new Vector3(0, 1, 0) * Time.deltaTime);
        }

        if(position >= minHeight && repeat)
        {
            rock.MovePosition(GetComponent<Rigidbody>().position + new Vector3(0, -1, 0) * Time.deltaTime);
        }

        if (position >= maxHeight)
        {
            repeat = true;
        }

        if (position <= minHeight)
        {
            repeat = false;
        }
    }
}
