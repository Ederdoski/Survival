﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants : MonoBehaviour {


	public static string TRUE = "TRUE";
	public static string FALSE = "FALSE";

    //---Tags

    public static string PLAYER = "Player";
    public static string BULLET_SHOTGUN = "Bullet_Shotgun";
    public static string BULLET_AK47 = "Bullet_Ak47";
    public static string BULLET_GRENADE = "Bullet_Grenade";

    public static string EASY_BOSS = "Boss_Easy";
    public static string MEDIUN_BOSS = "Boss_Mediun";
    public static string HARD_BOSS = "Boss_Hard";
    public static string ENTRY_BOSS = "Entry_Boss";

    public static string SHIELD = "Shield";
    public static string SPEED  = "Speed";
    public static string BULLET = "Bullet";
    public static string HEAL   = "Heal";

    public static string ATTACK_SPECIAL = "Special_Attack";
    public static string ATTACK_NORMAL = "Normal_Attack";
    public static string ATTACK_ULTI = "Ulti_Attack";

    //--- Scenes

    public static string SCENE_SURVIVAL    = "Survival";
    public static string SCENE_DEFEAT      = "defeat";
    public static string SCENE_MENU        = "init";
    public static string SCENE_BOSS_AREA   = "bossArea";
    public static string SCENE_BOSS_FIGHT  = "bossFight";


    //---Player

    public static float PLAYER_HEAL = 100;

    public static string GUN_AK47 = "AK47";
    public static string GUN_SHOTGUN = "Shotgun";
    public static string GUN_GRENADE_LAUNCHER = "Grenade_Launcher";

    public static int REWARD_GUN_AK47 = 2;
    public static int REWARD_GUN_GRENADE_LAUNCHER = 4;

    public static float DAMAGE_BULLET_SHOTGUN = 50f;
    public static float DAMAGE_BULLET_AK47 = 20f;
    public static float DAMAGE_GRENADE_LAUNCHER = 100f;

    public static float SPEED_PLAYER = 1f;
    public static float SPEED_BULLET_SHOTGUN = 30f;
    public static float SPEED_BULLET_AK47 = 40f;
    public static float SPEED_GRENADE_LAUNCHER = 15f;

    public static float DESTROY_EXPLODE_SHOTGUN = 0.8f;
    public static float DESTROY_BULLET = 1.5f;

    //---Enemy

    public static float LICH_HEAL   = 100;
    public static float TROLL_HEAL  = 100;
    public static float DRAGON_HEAL = 15000;

    public static int BASIC_ATTACK = 0;
    public static int SPECIAL_ATTACK = 1;

    public static int RANGE_LICH_SPECIAL = 17;
    public static int RANGE_LICH_NORMAL  = 12;
    public static int RANGE_TROLL_NORMAL = 3;
    public static int RANGE_DRAGON_NORMAL = 10;
    public static int RANGE_DRAGON_SPECIAL = 8;

    public static float COLDOWN_ATTACK_LICH  = 4f;
    public static float COLDOWN_ATTACK_TROLL = 1.5f;
    public static float COLDOWN_ATTACK_DRAGON = 4f;

    public static float BASIC_DAMAGE_ENEMY = 5f;

    public static float BASIC_DAMAGE_DRAGON = 10f;
    public static float SPECIAL_DAMAGE_DRAGON = 0.5f;
    public static float ULTI_DAMAGE_DRAGON = 20f;

    public static float DESTROY_BODY_ENEMY = 4f;
    public static float DESTROY_BODY_BOSS  = 20f;

    public static float MOVE_SPEED_TROLL = 1.5f;
    public static float MOVE_SPEED_LICH = 1.8f;
    public static float MOVE_SPEED_WALK_DRAGON = 3.5f;
    public static float MOVE_SPEED_RUN_DRAGON = 4.5f;
    public static float SPEED_FIREBALL = 35f;

    //--- Buff

    public static float MIN_TIME_SPAW_BUFF = 25f;
    public static float MAX_TIME_SPAW_BUFF = 45f;
    public static float TIME_DESTROY_BUFF  = 30f;

    public static float GAIN_BUFF_SPEED    = 1f;
    public static float GAIN_BUFF_HEAL     = 25f;
    public static float TIME_DURATION_BUFF = 15f;


    public static string ID_BUFF_SPEED  = "1";
    public static string ID_BUFF_SHIELD = "2";
    public static string ID_BUFF_BULLET = "3";


    //---Configuration

    public static float TIME_CHANGE_TIPS = 4;
    public static float TIME_SHOW_LOADING = 10;
    public static float ROUND_REFRESH = 5;
    public static float TIME_SPAWN = 40;
    public static float MIN_TIME_SPAWN = 15;
    public static float VOLUME_ACT = 0.7f;
    public static float SPEED_ROTATE_ANIMATIONS = 20f;


    //---Animation rocks

    public static float MAX_HEIGHT = 8.5f;
    public static float MIN_HEIGHT = 0;

    //---Animation skull

    public static float MAX_HEIGHT_SKULL = 30f;
    public static float MIN_HEIGHT_SKULL = 26.4f;


    //---Tips

    public static string [] ARRAY_TIPS = new string[] {"Los enemigos son mas fuertes de lo que parecen.", 
                                                       "Recuerda descansar de vez en cuando entre cada partida.",
                                                       "El lich es un mago muy poderoso, intenta evitar sus ataques a distancia.",
                                                       "El troll solo ataca cuerpo a cuerpo, no te acerques mucho a el.",
                                                       "Con la cantidad suficiente de muertes podras obtener nuevas armas.",
                                                       "Si derrotas a un boss, obtendras recompesas muy especiales.",
                                                       "Asegurate de ir preparado antes de enfrentar a un boss.",
                                                       "Existen 3 tipos de boss, cada uno es mas fuerte que el anterior.",
                                                       "Si te dejas alcanzar por la llama del -Dragon- te hara mucho daño",
                                                       "-Dragon- es un dragon solitario y no le gusta ser molestado."};






}
