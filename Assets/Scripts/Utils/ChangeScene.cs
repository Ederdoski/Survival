﻿using System.Collections;
using System.Collections.Generic;
using TouchControlsKit;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChangeScene : MonoBehaviour {

    public string Scene;

    void Update()
    {
        if (TCKInput.GetAction("ChangeSceneBtn", EActionEvent.Press))
        {
            changeScene();
        } 
    }

    void changeScene () {
            SceneManager.LoadScene(Scene, LoadSceneMode.Single);
    }
}
