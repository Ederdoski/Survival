﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAnimations : MonoBehaviour {

    public string eje;
    private float speed;

    void Awake(){
        speed = Constants.SPEED_ROTATE_ANIMATIONS;
    }

	void Update () {

        if (eje == "x")
        {
            transform.Rotate(new Vector3(1, 0, 0) * speed * Time.deltaTime);
        }

        if (eje == "z")
        {
            transform.Rotate(new Vector3(0,0,1) * speed * Time.deltaTime);
        }

        if (eje == "y")
        {
            transform.Rotate(new Vector3(0, 1, 0) * speed * Time.deltaTime);
        }
    }
}
