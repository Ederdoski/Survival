﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffSpawn : MonoBehaviour {

	public GameObject[] arrayBuffs;

	public float[] positionSpawn;

	private Vector3 position;

    private float timer;

    void Awake()
    {
        timer = Time.time + Random.Range(Constants.MIN_TIME_SPAW_BUFF, Constants.MAX_TIME_SPAW_BUFF);
    }

    void Update()
    {
        if (timer < Time.time) {

		   position = new Vector3(Random.Range(positionSpawn[0], positionSpawn[1]), 2.7f, Random.Range(positionSpawn[2], positionSpawn[3]));

           Instantiate(arrayBuffs[Random.Range(0, arrayBuffs.Length)], position, transform.rotation);

           timer = Time.time + Random.Range(Constants.MIN_TIME_SPAW_BUFF, Constants.MAX_TIME_SPAW_BUFF);
        }
    }


}
