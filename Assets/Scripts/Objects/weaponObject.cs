﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class weaponObject {

	public int id;
	public string name;
	public string status;
	public string isSelected;
	public float cdWeapon;
	public float rewardWeapon;
	public AudioClip sFire;
	public AudioClip sReload;
	public GameObject bullet;
	public GameObject weapon;

	public weaponObject(int _id, string _name, string _status, string _isSelected, float _cdWeapon, float _rewardWeapon, AudioClip _sFire, AudioClip _sReload, GameObject _bullet, GameObject _weapon)
    {
        id = _id;
        name      = _name;
		status    = _status;
		cdWeapon  = _cdWeapon;
		sFire     = _sFire;
		sReload   = _sReload;
		bullet    = _bullet;
		weapon    = _weapon;
		isSelected= _isSelected;
		rewardWeapon  = _rewardWeapon;
    }

	public int getID(){
    	return id;
    }

    public string getName(){
    	return name;
    }

	public AudioClip getSoundFire(){
		return sFire;
	}

	public AudioClip getSoundReload(){
		return sReload;
	}

	public GameObject getBulet(){
		return bullet;
	}

	public GameObject getWeapon(){
		return weapon;
	}

    public string getStatus(){
    	return status;
    }

	public string getIsSelected(){
		return isSelected;
	}

	public void setStatus(string _status){
		status = _status;
	}

	public void setIsSelected(string _selected){
		isSelected = _selected;
	}

	public void setWeapon(GameObject _weapon){
		weapon = _weapon;
	}

	public float getCDWeapon(){
		return cdWeapon;
	}

	public float getRewardWeapon(){
		return rewardWeapon;
	}
}
