﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ani : MonoBehaviour {

	public GameObject lich;

	public GameObject lich2;


	private GameObject skill;
	private GameObject skill1;
	private GameObject skill2;

	private float coldown;
	private bool ischange = false;
	private bool ischange1 = false;
	private bool ischange2 = false;



	// Update is called once per frame
	void Update () {

		coldown = Coldown.add(coldown);

		if(coldown > 10 && !ischange){

            skill1 = Instantiate(lich, new Vector3(-1.87f, 5.078569e-17f, -0.46f), transform.rotation) as GameObject;
            skill = Instantiate(lich, new Vector3(-0.2499999f, 5.078569e-17f, -0.46f), transform.rotation) as GameObject;
            skill2 = Instantiate(lich, new Vector3(1.44f, 5.078569e-17f, -0.46f), transform.rotation) as GameObject;
            ischange = true;
		}

		if(coldown > 30 && !ischange1){
			Destroy(skill);
			Destroy(skill1);
			Destroy(skill2);
			ischange1 = true;
		}

		if(coldown > 35 && !ischange2){
			ischange2 = true;
			//SASA
            skill1 = Instantiate(lich2, new Vector3(-1.87f, 5.078569e-17f, -0.46f), transform.rotation) as GameObject;
            skill = Instantiate(lich2, new Vector3(-0.2499999f, 5.078569e-17f, -0.46f), transform.rotation) as GameObject;
            skill2 = Instantiate(lich2, new Vector3(1.44f, 5.078569e-17f, -0.46f), transform.rotation) as GameObject;

		}
		
	}
}
