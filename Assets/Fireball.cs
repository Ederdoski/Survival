﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour {

    public GameObject animExplotion;

    private float cdFireball;

    void Update(){
    	cdFireball = Coldown.add(cdFireball);
    }

	void FixedUpdate()
    {
        GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + transform.forward * Constants.SPEED_FIREBALL * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Wall" && cdFireball > 0.5f)
        {
        	cdFireball = Coldown.reset();
            Instantiate(animExplotion, transform.position, transform.rotation);
            destroyFireball();
        }

        if (other.gameObject.tag == Constants.PLAYER && cdFireball > 0.5f)
        {
            Instantiate(animExplotion, transform.position, transform.rotation);

        	cdFireball = Coldown.reset();
        	PlayerUI.addDamage(Constants.ULTI_DAMAGE_DRAGON);
            destroyFireball();
        }
    }

    void destroyFireball()
    {
        Destroy(gameObject);
    }
}
